package pe.com.tpp.ac.camunda.dao;

import org.apache.ibatis.annotations.*;
import pe.com.tpp.ac.camunda.model.Task;
import pe.com.tpp.ac.camunda.model.TaskDetail;

import java.util.ArrayList;

@Mapper
public interface TaskMapper {

    @Insert("INSERT INTO transversal.tbl_mge_tareas(cpnid_transaccion, cpcdescripcion, " +
            "cpclinktransaccion, cpnid_estadoregistro, cpcusuario) " +
            "VALUES ( " +
            "#{body.transaction}, #{body.description}, #{body.linkTransaction}, " +
            "#{body.registrationStatus}, #{body.user});")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insertTask(@Param("body") Task body);



    @Select("SELECT cpnid_tareas,\n" +
            "\t   cpnid_transaccion, cpcdescripcion,\n" +
            "\t   cpclinktransaccion, cpnid_estadoregistro, cpcusuario, cpdcreacion\n" +
            "\tFROM transversal.tbl_mge_tareas\n" +
            "\twhere cpnid_estadoregistro = 1;")
    ArrayList<Task> listTaks();


    @Insert("INSERT INTO transversal.tbl_com_tareas( " +
            "cpnid_tareas, " +
            "cpnid_transaccion, " +
            "cpcdescripcion, " +
            "cpcgrupo_asignado, " +
            "cpcusuario_asignado, " +
            "cpdfechalimite, " +
            "cpcarchivo, " +
            "cpccomentario, " +
            "cpnid_sustento, " +
            "cpcprocesocamundaid, " +
            "cpcpasocamundaid, " +
            "cpnid_estadotarea, " +
            "cpnid_estadoregistro, " +
            "cpcusuario " +
            ") VALUES ( " +
            "#{body.task}, " +
            "#{body.transaction}, " +
            "#{body.description}, " +
            "#{body.assignedGroup}, " +
            "#{body.assignedUser}, " +
            "#{body.deadline}, " +
            "#{body.file}, " +
            "#{body.commentary}, " +
            "#{body.support}, " +
            "#{body.camundaProcess}, " +
            "#{body.stepCamunda}, " +
            "#{body.taskStatus}, " +
            "#{body.registrationStatus}, " +
            "#{body.user})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insertTaskDetail(@Param("body") TaskDetail body);

}
