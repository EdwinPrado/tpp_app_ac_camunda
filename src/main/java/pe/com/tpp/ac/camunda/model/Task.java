package pe.com.tpp.ac.camunda.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class Task {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("transaction")
    private Integer transaction;

    @JsonProperty("description")
    private String description;

    @JsonProperty("linkTransaction")
    private String linkTransaction;

    @JsonProperty("registrationStatus")
    private Integer registrationStatus;

    @JsonProperty("user")
    private String user;

    @JsonProperty("registrationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date registrationDate;
}
