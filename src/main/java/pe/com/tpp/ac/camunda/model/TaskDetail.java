package pe.com.tpp.ac.camunda.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class TaskDetail {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("task")
    private Integer task;

    @JsonProperty("transaction")
    private Integer transaction;

    @JsonProperty("description")
    private String description;

    @JsonProperty("assignedGroup")
    private String assignedGroup;

    @JsonProperty("assignedUser")
    private String assignedUser;

    @JsonProperty("deadline")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date deadline;

    @JsonProperty("file")
    private String file;

    @JsonProperty("commentary")
    private String commentary;

    @JsonProperty("support")
    private String support;

    @JsonProperty("camundaProcess")
    private String camundaProcess;

    @JsonProperty("stepCamunda")
    private String stepCamunda;

    @JsonProperty("taskStatus")
    private Integer taskStatus;

    @JsonProperty("registrationStatus")
    private Integer registrationStatus;

    @JsonProperty("user")
    private String user;

    @JsonProperty("registrationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date registrationDate;

}
