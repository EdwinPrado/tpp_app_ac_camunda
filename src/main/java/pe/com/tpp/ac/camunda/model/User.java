package pe.com.tpp.ac.camunda.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class User {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("person")
    private Integer person;

    @JsonProperty("externalUser")
    private Integer externalUser;

    @JsonProperty("md5Key")
    private Integer md5Key;

}
