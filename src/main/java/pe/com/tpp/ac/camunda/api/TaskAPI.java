package pe.com.tpp.ac.camunda.api;

import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.tpp.ac.camunda.model.ErrorHttp;
import pe.com.tpp.ac.camunda.model.Task;
import pe.com.tpp.ac.camunda.model.TaskDetail;
import pe.com.tpp.ac.camunda.service.TaskService;
import pe.com.tpp.ac.camunda.util.Constants;
import pe.com.tpp.ac.camunda.util.TimeLoggerAgent;
import pe.com.tpp.ac.camunda.util.TimeRecord;

import java.util.UUID;

@CrossOrigin(origins = "*", maxAge = 3600, methods= {RequestMethod.GET,RequestMethod.POST})
@RestController
@RequestMapping("/v1/task")
@ApiResponse(responseCode = "401",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Authentication Failure")
@ApiResponse(responseCode = "400",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Hay un elemento incorrecto")
@ApiResponse(responseCode = "404",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Hay un elemento incorrecto")
@ApiResponse(responseCode = "500",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Hay un elemento incorrecto")
@SecurityRequirement(name = "TPP")

public class TaskAPI {

    @Autowired
    TaskService taskService;
    @Autowired
    private TimeRecord timeRecord;
    @Autowired
    private TimeLoggerAgent timeLoggerAgent;

    @ApiResponse(responseCode = "200",
            headers = {
                    @Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))},
            content = @Content(mediaType = "application/json",   schema = @Schema(implementation = TaskDetail.class)))
    @PostMapping(value = "/addTaskDetail",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskDetail> saveTaskDetail(@RequestBody TaskDetail body){
        String uuid = UUID.randomUUID().toString();
        MDC.put(Constants.CORRELATION_ID_MDC, uuid);
        timeRecord.setCorrelationId(uuid);
        timeRecord.setType(Constants.CODE_INFO_LOG);
        timeRecord.setEndPoint("/addTaskDetail");
        timeRecord.setStartTime(System.currentTimeMillis());

        TaskDetail taskDetail = taskService.insertTaskDetail(body);

        timeRecord.setStatus(Constants.CODE_OK_200);
        timeRecord.setEndTime(System.currentTimeMillis());
        timeLoggerAgent.logService(timeRecord);
        return new ResponseEntity<>(taskDetail, HttpStatus.OK);
    }

}
