package pe.com.tpp.ac.camunda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TppAppAcCamundaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TppAppAcCamundaApplication.class, args);
	}

}
