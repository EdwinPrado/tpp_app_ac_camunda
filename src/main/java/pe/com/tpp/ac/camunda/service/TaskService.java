package pe.com.tpp.ac.camunda.service;

import pe.com.tpp.ac.camunda.model.Task;
import pe.com.tpp.ac.camunda.model.TaskDetail;

public interface TaskService {

    Task insertTask (Task body);
    TaskDetail insertTaskDetail (TaskDetail body);
}
