package pe.com.tpp.ac.camunda.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tpp.ac.camunda.dao.TaskMapper;
import pe.com.tpp.ac.camunda.model.Task;
import pe.com.tpp.ac.camunda.model.TaskDetail;
import pe.com.tpp.ac.camunda.service.TaskService;

@Service("TaskService")
public class TaskServiceImpl implements TaskService {

    @Autowired
    TaskMapper taskMapper;

    @Override
    public Task insertTask(Task body) {
        taskMapper.insertTask(body);
        return body;
    }

    @Override
    public TaskDetail insertTaskDetail(TaskDetail body) {
        taskMapper.insertTaskDetail(body);
        return body;
    }
}
